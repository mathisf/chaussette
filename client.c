#if defined(WIN32)
#include <winsock2.h>
typedef int socklen_t;
#else
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define closesocket(s) close(s)
typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
#endif

#include "utils/string_utils.h"
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MESSAGE_LENGTH 1024

int main(int argc, char *argv[]) {
#if defined(WIN32)
  WSADATA WSAData;
  int error = WSAStartup(MAKEWORD(2, 2), &WSAData);
#else
  int error = 0;
#endif

  pid_t pid_send_message = 0;

  SOCKET sock;
  SOCKADDR_IN sin;
  char buffer[MESSAGE_LENGTH] = "";
  char message[MESSAGE_LENGTH];

  int recv_code = 1;

  if (argc == 3) {
    /* configure the ip address and port*/
    sin.sin_addr.s_addr = inet_addr(argv[1]);
    sin.sin_family = AF_INET;
    sin.sin_port = htons(atoi(argv[2]));
  } else if (argc > 3) {
    printf("Trop de paramètres (paramètres requis : <adresse_ip> <port>).\n");
    return -1;
  } else {
    printf(
        "Pas assez de paramètre (paramètres requis : <adresse_ip> <port>).\n");
    return -1;
  }

  /* if the socket is working */
  if (!error) {
    /* create a socket */
    sock = socket(AF_INET, SOCK_STREAM, 0);

    /* connect to the ip address and port and bind to the socket */
    if (connect(sock, (SOCKADDR *)&sin, sizeof(sin)) != SOCKET_ERROR) {
      reset_string(message);
      printf("Connection à %s sur le port %d\n", inet_ntoa(sin.sin_addr),
             htons(sin.sin_port));
      pid_send_message = fork();

      // process that send message
      if (pid_send_message == 0) {
        while (1) {
          // If we write something and press 'Enter'
          if (strcmp(fgets(message, MESSAGE_LENGTH, stdin), "") != 0) {
            // remove the excess spaces at the start and at the end of the
            // message
            trim(message);
            // send the message
            send(sock, message, strlen(message) + 1, 0);
            reset_string(message);
            printf(MOVE_UP);
            printf(DELETE_LINE);
          }
        }
        // process that receive messages
      } else {
        reset_string(buffer);
        while (recv_code != 0 && recv_code != SOCKET_ERROR) {
          if (strcmp(buffer, "") != 0) {
            // clean the line and print the received message
            printf("\r%s\n", buffer);
            fflush(stdout);
            reset_string(buffer);
          }
          // receive a message from the server (if there is one)
          recv_code = recv(sock, buffer, MESSAGE_LENGTH, 0);
        }
        printf("La connexion au serveur a été intérrompue.\n");
      }
    } else {
      printf("Impossible de se connecter\n");
      exit(1);
    }

    // close the socket
    closesocket(sock);

#if defined(WIN32)
    WSACleanup();
#endif
  }

  return EXIT_SUCCESS;
}