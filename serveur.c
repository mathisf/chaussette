#include "utils/string_utils.h"
#include "utils/time_utils.h"
#include <arpa/inet.h>
#include <ctype.h>
#include <errno.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

// Constants
#define MESSAGE_LENGTH 1024
#define MAX_CLIENT 128
#define USERNAME_LENGTH 30

// Client structure
struct client {
  int socket;
  char username[100];
  char color[16];
};

// Generate a string of connected clients
char *generate_list(struct client clients[MAX_CLIENT]) {
  char *result = malloc(sizeof(char *) * MESSAGE_LENGTH);
  strncpy(result, "--- LISTE DES UTILISATEURS ---\n\n", 33);
  char tmp[USERNAME_LENGTH + 107];
  for (int i = 0; i < MAX_CLIENT; i++) {
    if (clients[i].socket > 0) {
      snprintf(tmp, 136, "   %d. %s%s%s\n", i + 1, clients[i].color,
               clients[i].username, ANSI_COLOR_RESET);
      strncat(result, tmp, 50);
    }
  }
  strncat(result, "\n------------------------------\0", 33);
  return result;
}

// Close the client socket, resets the socket, username and color
void kick_client(struct client *client, int index) {
  close(client->socket);
  client->socket = 0;
  snprintf(client->username, USERNAME_LENGTH, "Invité(%d)", index + 1);
  snprintf(client->color, 16, "\x1b[%dm", rand() % ((36 + 1) - 31) + 31);
}

// Sets a new username to a client, if it's not already taken
bool set_user_name(struct client *client, char *username,
                   struct client clients[MAX_CLIENT]) {
  remove_space(username);
  bool existe_deja = false;
  for (int i = 0; i < MAX_CLIENT; ++i) {
    if (strncmp(clients[i].username, username, USERNAME_LENGTH) == 0) {
      existe_deja = true;
      break;
    }
  }

  if (!existe_deja) {
    strncpy(client->username, username, USERNAME_LENGTH);
  }

  return existe_deja;
}

// Sets a new color to a client
bool set_color(struct client *client, char *color) {
  bool doesnt_exist = false;
  if (strcmp(color, "red") == 0) {
    strcpy(client->color, ANSI_COLOR_RED);
  } else if (strcmp(color, "blue") == 0) {
    strcpy(client->color, ANSI_COLOR_BLUE);
  } else if (strcmp(color, "green") == 0) {
    strcpy(client->color, ANSI_COLOR_GREEN);
  } else if (strcmp(color, "yellow") == 0) {
    strcpy(client->color, ANSI_COLOR_YELLOW);
  } else if (strcmp(color, "magenta") == 0) {
    strcpy(client->color, ANSI_COLOR_MAGENTA);
  } else if (strcmp(color, "cyan") == 0) {
    strcpy(client->color, ANSI_COLOR_CYAN);
  } else if (strcmp(color, "white") == 0) {
    strcpy(client->color, ANSI_COLOR_RESET);
  } else {
    doesnt_exist = true;
  }

  return doesnt_exist;
}

// Send a private message to someone, if he exists and if he has a socket
bool whisper_message(char message[MESSAGE_LENGTH],
                     struct client clients[MAX_CLIENT], struct client sender,
                     char *recipient_username) {
  char message_tmp[MESSAGE_LENGTH];
  bool existe = false;
  int recipient_socket = 0;

  for (int i = 0; i < MAX_CLIENT; ++i) {
    if (strncmp(clients[i].username, recipient_username, USERNAME_LENGTH) ==
        0) {
      existe = true;
      recipient_socket = clients[i].socket;
      break;
    }
  }

  if (existe && recipient_socket > 0) {
    snprintf(message_tmp, MESSAGE_LENGTH, "%s [CHUCHOTEMENT] %s%s%s %s",
             get_time("%H:%M:%S"), sender.color, sender.username,
             ANSI_COLOR_RESET, message);
    send(recipient_socket, message_tmp, strlen(message_tmp) + 1, 0);
  }

  return !(existe && recipient_socket > 0);
}

// Send a message to a client
void send_message(char message[MESSAGE_LENGTH], struct client client) {
  send(client.socket, message, strlen(message) + 1, 0);
}

// Extract a command to execute from a message
// Uses a message (string), the sender client, the list of clients and the index
//  of the sender client in the list of clients
void execute_command(char message[MESSAGE_LENGTH], struct client *client,
                     struct client *clients, int clientIndex) {
  struct splitted_string args;
  char feedback[MESSAGE_LENGTH];
  if (strncmp(message, "/setusername ", 13) == 0) {
    args = str_split(message, 2, ' ');
    if (set_user_name(client, args.array[1], clients)) {
      strncpy(feedback,
              "[SERVEUR] Erreur : Ce nom d'utilisateur est déjà utilisé.", 61);
    } else {
      strncpy(feedback, "[SERVEUR] Nom d'utilisateur modifié.", 38);
    }
  } else if (strncmp(message, "/setcolor ", 10) == 0) {
    args = str_split(message, 2, ' ');
    if (set_color(client, args.array[1])) {
      strncpy(feedback, "[SERVEUR] Erreur : Cette couleur n'existe pas.", 47);
    } else {
      strncpy(feedback, "[SERVEUR] Couleur modifiée.", 29);
    }
  } else if (strncmp(message, "/whisper ", 9) == 0) {
    args = str_split(message, 3, ' ');
    if (args.number_of_elements == 3 &&
        whisper_message(args.array[2], clients, *client, args.array[1])) {
      strncpy(feedback,
              "[SERVEUR] Erreur : L'utilisateur est introuvable ou le "
              "message est vide.",
              73);
    } else {
      strncpy(feedback, "[SERVEUR] Message envoyé.", 27);
    }
  } else if (strncmp(message, "/exit", 5) == 0 ||
             strncmp(message, "/quit", 5) == 0) {
    kick_client(client, clientIndex);
  } else if (strncmp(message, "/list", 5) == 0) {
    send_message(generate_list(clients), *client);
  } else if (strncmp(message, "/help", 5) == 0) {
    strncpy(feedback,
            "[SERVEUR] Commandes disponibles : \n\t/setusername [username], "
            "\n\t/setcolor [red|blue|green|yellow|cyan|magenta|white], "
            "\n\t/whisper [username] [message], \n\t/list, \n\t/quit, "
            "\n\t/exit, \n\t/help",
            185);
  } else {
    strncpy(feedback,
            "[SERVEUR] Cette commande n'existe pas, commandes disponibles : "
            "\n\t/setusername [username], \n\t/setcolor "
            "[red|blue|green|yellow|cyan|magenta|white], \n\t/whisper "
            "[username] [message], \n\t/list, \n\t/quit, \n\t/exit, \n\t/help",
            214);
  }
  if (strcmp(feedback, "") != 0) {
    send_message(feedback, *client);
  }
}

int main(int argc, char *argv[]) {
  int port;
  if (argc == 2) {
    port = atoi(argv[1]);
  } else if (argc > 2) {
    printf("Trop de paramètres (paramètres requis : <port>).\n");
    return -1;
  } else {
    printf("Pas assez de paramètre (paramètres requis : <port>).\n");
    return -1;
  }

  int opt = 1; // true

  // Clients list
  struct client clients[MAX_CLIENT];

  int master_socket, addrlen, new_socket, activity, i, valread, sd, sd2;
  int max_sd;
  struct sockaddr_in address;

  char buffer[MESSAGE_LENGTH];
  char buffer_tmp[MESSAGE_LENGTH];

  // Used
  char previous_color[16] = "";
  char color[16] = "";

  // set of socket descriptors
  fd_set readfds;

  // a message
  char welcome_message[MESSAGE_LENGTH] =
      "[SERVEUR] Bienvenue sur le serveur de chat en ligne. Utilisez /help "
      "pour avoir la liste de commandes disponibles.";
  struct splitted_string splitted_received_message;

  // set the random number generator
  srand(time(NULL));

  // initialise all client_sockets to 0
  for (i = 0; i < MAX_CLIENT; i++) {
    clients[i].socket = 0;
    snprintf(clients[i].username, 100, "Invité(%d)", i + 1);
    do {
      snprintf(color, 16, "\x1b[%dm", rand() % ((36 + 1) - 31) + 31);
    } while (strcmp(color, previous_color) == 0);
    strcpy(previous_color, color);
    strcpy(clients[i].color, color);
  }

  // create a master socket
  if ((master_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
    perror("Création du socket échoué");
    exit(EXIT_FAILURE);
  }

  // set master socket to allow multiple connections ,
  // this is just a good habit, it will work without this
  if (setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt,
                 sizeof(opt)) < 0) {
    perror("Autorisation de multiple connexions échouée");
    exit(EXIT_FAILURE);
  }

  // type of socket created
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(port);

  // bind the socket to localhost
  if (bind(master_socket, (struct sockaddr *)&address, sizeof(address)) < 0) {
    perror("Liaison échouée.");
    exit(EXIT_FAILURE);
  }
  printf("En attente de connection sur le port %d... \n", port);

  // try to specify maximum of 5 pending connections for the master socket
  if (listen(master_socket, 5) < 0) {
    perror("listen");
    exit(EXIT_FAILURE);
  }

  addrlen = sizeof(address);

  while (1) {
    // clear the socket set
    FD_ZERO(&readfds);

    // add master socket to set
    FD_SET(master_socket, &readfds);
    max_sd = master_socket;

    // add child sockets to set
    for (i = 0; i < MAX_CLIENT; i++) {
      // socket descriptor
      sd = clients[i].socket;

      // if valid socket descriptor then add to read list
      if (sd > 0)
        FD_SET(sd, &readfds);

      // highest file descriptor number, need it for the select function
      if (sd > max_sd)
        max_sd = sd;
    }

    // wait for an activity on one of the sockets, timeout is NULL,
    // so wait indefinitely
    activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);

    if ((activity < 0) && (errno != EINTR)) {
      printf("select");
    }

    // If something happened on the master socket,
    // then its an incoming connection
    if (FD_ISSET(master_socket, &readfds)) {
      if ((new_socket = accept(master_socket, (struct sockaddr *)&address,
                               (socklen_t *)&addrlen)) < 0) {
        perror("accept");
        exit(EXIT_FAILURE);
      }

      // inform user of socket number
      printf("Nouvelle connexion au socket avec le numéro %d, l'IP est : %s et "
             "le port est : %d \n",
             new_socket, inet_ntoa(address.sin_addr), ntohs(address.sin_port));

      // send new connection welcome message
      if (send(new_socket, welcome_message, strlen(welcome_message) + 1, 0) !=
          strlen(welcome_message) + 1) {
        perror("send");
      }

      // add new socket to array of sockets
      for (i = 0; i < MAX_CLIENT; i++) {
        // if position is empty
        if (clients[i].socket == 0) {
          clients[i].socket = new_socket;

          break;
        }
      }
    }

    // else its some IO operation on some other socket
    for (i = 0; i < MAX_CLIENT; i++) {
      sd = clients[i].socket;

      // if something happens on sd
      if (FD_ISSET(sd, &readfds)) {
        // if its a disconnect message
        if ((valread = read(sd, buffer, MESSAGE_LENGTH)) == 0) {
          // someone is disconnected
          getpeername(sd, (struct sockaddr *)&address, (socklen_t *)&addrlen);
          printf("Le client avec l'IP %s et le port %d s'est déconnecté\n",
                 inet_ntoa(address.sin_addr), ntohs(address.sin_port));

          // close his socket reset his username and client
          kick_client(&clients[i], i);
        }

        // its a regular message or a command
        else {
          // buffer is the message
          // check if the message is empty
          if (strcmp(buffer, "\n") != 0 && strcmp(buffer, "") != 0) {
            // End of the string
            buffer[valread] = '\0';
            // remove special characters (not working)
            validate_string(buffer);
            // if the message starts with a forward slash
            if (buffer[0] == '/') {
              execute_command(buffer, &clients[i], clients, i);
              reset_string(buffer);
              break;
            } else {
              for (int j = 0; j < MAX_CLIENT; j++) {
                sd2 = clients[j].socket;
                if (sd2 > 0) {
                  snprintf(buffer_tmp, MESSAGE_LENGTH, "%s %s%s%s %s",
                           get_time("%H:%M:%S"), clients[i].color,
                           clients[i].username, ANSI_COLOR_RESET, buffer);
                  send(sd2, buffer_tmp, strlen(buffer_tmp) + 1, 0);
                  reset_string(buffer_tmp);
                }
              }
            }
            reset_string(buffer);
          }
          reset_string(buffer);
        }
      }
    }
  }

  return 0;
}
