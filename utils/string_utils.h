#include <assert.h>
#include <ctype.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_BLUE "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_RESET "\x1b[0m"

#define MOVE_UP "\033[1A"
#define MOVE_DOWN "\033[1B"
#define DELETE_LINE "\033[K"

#define MAX_STR_LENGTH 4095

#define KEY_UP 72
#define KEY_DOWN 80
#define KEY_LEFT 75
#define KEY_RIGHT 77

struct splitted_string {
  char **array;
  int number_of_elements;
};

char *substr(char *src, int pos, int len) {
  char *dest = NULL;
  if (len > 0) {

    dest = calloc(len + 1, 1);

    if (NULL != dest) {
      strncat(dest, src + pos, len);
    }
  }
  return dest;
}

void remove_space(char *source) {
  char *i = source;
  char *j = source;
  while (*j != 0) {
    *i = *j++;
    if (*i != ' ')
      i++;
  }
  *i = 0;
}

void delete_char_at_index(char *string, int index) {
  memmove(&string[index], &string[index + 1], strlen(string) - index);
}

void reset_string(char *string) {
  size_t size = sizeof(string);
  for (size_t i = 0; i < size; i++) {
    string[i] = '\0';
  }
}

void validate_string(char *string) {
  // int invalid = 0;
  // for (int index = 0; index < strlen(string); index++) {
  //   if (string[index] == KEY_UP) {
  //     invalid = 1;
  //   } else if (string[index] == KEY_DOWN) {
  //     invalid = 1;
  //   } else if (string[index] == KEY_LEFT) {
  //     invalid = 1;
  //   } else if (string[index] == KEY_RIGHT) {
  //     invalid = 1;
  //   } else {
  //     invalid = 0;
  //   }
  //   if (invalid == 1) {
  //       delete_char_at_index(string, index--);
  //   }
  // }
}

struct splitted_string str_split(char *string_to_split, int number_of_items,
                                 const char delimiter) {
  struct splitted_string result;
  result.array = 0;
  size_t count = 0;
  char *tmp = string_to_split;
  char *last_delim = 0;
  char delim[2];
  int current_number_of_items = 0;
  delim[0] = delimiter;
  delim[1] = 0;

  /* Count how many elements will be extracted. */
  while (*tmp) {
    if (delimiter == *tmp) {
      count++;
      last_delim = tmp;
    }
    tmp++;
  }

  /* Add space for trailing token. */
  count += last_delim < (string_to_split + strlen(string_to_split) - 1);

  /* Add space for terminating null string so caller
     knows where the list of returned strings ends. */
  count++;

  result.array = malloc((sizeof(char *) * count));

  if (result.array) {
    size_t idx = 0;
    char *token = strtok(string_to_split, delim);

    while (token && current_number_of_items < number_of_items) {
      assert(idx < count);
      *(result.array + idx++) = strndup(token, MAX_STR_LENGTH);
      token = strtok(0, delim);
      current_number_of_items++;
    }

    // Populate last element with all the remaining strings
    while (token) {
      strcat(result.array[current_number_of_items - 1], " ");
      strcat(result.array[current_number_of_items - 1], token);
      token = strtok(0, delim);
    }

    *(result.array + idx) = 0;
  }
  result.number_of_elements = current_number_of_items;
  return result;
}

void trim(char *input) {
  char *dst = input, *src = input;
  char *end;

  // Skip whitespace at front...
  while (isspace((unsigned char)*src)) {
    ++src;
  }

  // Trim at end...
  end = src + strlen(src) - 1;
  while (end > src && isspace((unsigned char)*end)) {
    *end-- = 0;
  }

  // Move if needed.
  if (src != dst) {
    while ((*dst++ = *src++))
      ;
  }
}
