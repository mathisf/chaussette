#include <stdio.h>
#include <time.h>

// Example format : %Y-%m-%d %H:%M:%S
char *get_time(char *format) {
    time_t timer;
    char *buffer = malloc(sizeof(char *) * 30);
    struct tm* tm_info;

    time(&timer);
    tm_info = localtime(&timer);

    strftime(buffer, 26, format, tm_info);
    return buffer;
}
