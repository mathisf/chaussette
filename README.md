# Chaussette - Multi-client chat

## Compilation (with GCC)

- `$ gcc serveur.c -o serveur.out`
- `$ gcc client.c -o client.out`

## Execution

- `$ ./serveur.out <port>`
- `$ ./client.out <address> <port>`

## Example

In one window/tab :

- `$ ./serveur.out 8888`

In another window/tab :

- `$ ./client.out 127.0.0.1 8888`

You can then set your username, your color and chat with other clients.

## List of commands

- `/setusername <username>` to change your username ;
- `/setcolor <red|blue|green|yellow|cyan|magenta|white>` to change the color of your username ;
- `/whisper <username> <message>` to whisper a message to someone (a private message) ;
- `/list` prints a list of connected clients ;
- `/help` shows the list of commands ;
- `/quit` disconnects the client ;
- `/exit` disconnects the client ;
